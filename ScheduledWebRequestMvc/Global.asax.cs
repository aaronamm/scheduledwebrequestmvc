﻿using System.Web.Http;
using System.Web.Mvc;
using System.Web.Routing;
using Quartz;
using Quartz.Impl;
using ScheduledWebRequestMvc.App_Start;

namespace ScheduledWebRequestMvc
{
    // Note: For instructions on enabling IIS6 or IIS7 classic mode, 
    // visit http://go.microsoft.com/?LinkId=9394801
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();

            WebApiConfig.Register(GlobalConfiguration.Configuration);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);

            ConfigJobSchedule();
        }

        private void ConfigJobSchedule()
        {
            var schedulerFactory = new StdSchedulerFactory();
            var scheduler = schedulerFactory.GetScheduler();
            scheduler.Start();


            var pingJob = JobBuilder.Create<PingJob>().Build();
            pingJob.JobDataMap.Add("url", "http://google.com");

            var trigger = TriggerBuilder.Create()
                .WithIdentity("pingJob")
                .WithSimpleSchedule( x => x.WithIntervalInSeconds(10)
                    .RepeatForever())
                    .Build();
            scheduler.ScheduleJob(pingJob, trigger);

        }
    }
}