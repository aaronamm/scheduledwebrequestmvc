﻿using System;
using System.Net;
using Quartz;

namespace ScheduledWebRequestMvc
{
    public class PingJob : IJob
    {

        public void Execute(IJobExecutionContext context)
        {

            try
            {
                var url = new Uri(context.MergedJobDataMap.GetString("url"));
                var webRequest = (HttpWebRequest) WebRequest.Create(url);
                webRequest.ContentType = "text/html";
                webRequest.Method = "GET";
                var httpResponse = (HttpWebResponse) webRequest.GetResponse();

                if (httpResponse.StatusCode != HttpStatusCode.OK)
                {
                    //TODO log error 
                }

            }
            catch (Exception ex)
            {
                //TODO log error 
            }
        }

    }//end class
}
